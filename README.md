Tasks
Start server
Requires running apache tomcat link to download: https://tomcat.apache.org/download-80.cgi
Requires running mysql server need url username password for database
Tables for database given below


Account GET/POST/PUT
Contact GET/POST/PUT
Contacts for account
all contacts for account
Test cases

API
	Java
	Apache Tomcat
	Jersey libraries
	
DATABASE

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `email_address` varchar(64) DEFAULT NULL,
  `address_1` varchar(64) DEFAULT NULL,
  `address_2` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
	
	
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(64) DEFAULT NULL,
  `address_1` varchar(64) DEFAULT NULL,
  `address_2` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
