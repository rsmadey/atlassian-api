package api;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@Path("/Account")
public class MarketPlaceAPI {
	
	
	

	@GET
	@Produces(MediaType.TEXT_HTML)
	public String confirmConnHtml(@QueryParam("type") String type,
			@QueryParam("method") String APImethod,
			@QueryParam("id") String id, 
			@QueryParam("match") String match, 
			@QueryParam("datamatch") String datamatch) {
		SqlConnection conn =  new SqlConnection();
		JsonObject response =  Json.createObjectBuilder().build();
		if(type.contains("contacts")) {
			if(APImethod.contains("get")) {
				response = conn.getContact(id, match, datamatch);
			}else if(APImethod.contains("put")) {
				response =  Json.createObjectBuilder().build();
			}else if(APImethod.contains("post")) {
				response =  Json.createObjectBuilder().build();	
			}
		}else {
			if(APImethod.contains("get")) {
				response = conn.getAccount(id);
			}else if(APImethod.contains("put")) {
				response =  Json.createObjectBuilder().build();
				
			}else if(APImethod.contains("post")) {
				response =  Json.createObjectBuilder().build();
			}
		}
		return response.toString();
	}
	
	/*@GET
	@Produces(MediaType.TEXT_HTML)
	public String postAccount(@QueryParam("company") String company, 
			@QueryParam("address1") String address1, 
			@QueryParam("address2") String address2, 
			@QueryParam("city") String city, 
			@QueryParam("state") String state, 
			@QueryParam("postal") String postal_code, 
			@QueryParam("country") String country) {
		SqlConnection conn = new SqlConnection();
		String response = conn.postAccount(company, address1, address2, city, state, postal_code, country);
		return response;
	}*/

}
