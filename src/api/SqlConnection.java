package api;

import java.sql.*;
import java.util.Arrays;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.glassfish.json.*;

public class SqlConnection {
	private Connection con;
	private Statement st;
	private ResultSet rs;
	
	public SqlConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atlassian", "root", "AtlassianAPI2018!");
			st = con.createStatement();
			
		}catch(Exception ex) {
			//exc.printStackTrace();
			System.out.println("Erro: "+ex);
		}
	}
	//retrieve all account data based on account id//
	public JsonObject getAccount(String id) {
		String query = "SELECT * FROM account WHERE id = " + id;
		JsonObject response = Json.createObjectBuilder().build();;
		try {
			rs = st.executeQuery(query);
			while(rs.next()) {
				response = Json.createObjectBuilder()
					.add("company_name", rs.getString("company_name"))
					.add("address_1", rs.getString("address_1"))
					.add("address_2", rs.getString("address_2"))
					.add("city", rs.getString("city"))
					.add("state", rs.getString("state"))
					.add("postal", rs.getString("postal_code"))
					.add("country", rs.getString("country")).build();
				;
			}
		}catch(Exception ex) {
			System.out.println(ex);
		}
		return response;
	}
	//insert new account into database
	public String postAccount(String company, String address1, String address2, String city, String state, String postal_code, String country) {
		String query = "INSERT INTO account (company_name, address_1, address_2, city, state, postal_code, country) values ("
				+ company + ","
				+ address1 + ","
				+ address2 + ","
				+ city + ","
				+ state + ","
				+ postal_code + ","
				+ country + "," 
				+")";
		try {
			rs = st.executeQuery(query);
			return "success";
		}catch(Exception ex) {
			return "failed";
		}
		
	}
	//update account based on account id
	/*public String putAccount(String accountID, String column, String newData) {
		
		String Query = "UPDATE account SET " + + " WHERE id = " + accountID;
		try {
			
		}catch(Exception ex) {
			return "failed";
		}
		
	}*/
	public JsonObject getContact(String accountID, String columnMatch, String dataMatch) {
		String query;
		
		if(columnMatch.contains("all")) {
			query = "SELECT * FROM contact WHERE account_id = " + accountID;
		}else {
			query = "SELECT * FROM contact WHERE account_id = " + accountID + " AND " + columnMatch + " = \"" + dataMatch + '"';
		}
		JsonObject response = Json.createObjectBuilder().add("query",query).build();
		
		try {
			rs = st.executeQuery(query);
			while(rs.next()) {
				response = Json.createObjectBuilder()
					.add("name", rs.getString("name"))
					.add("address_1", rs.getString("address_1"))
					.add("address_2", rs.getString("address_2"))
					.add("city", rs.getString("city"))
					.add("state", rs.getString("state"))
					.add("postal", rs.getString("postal_code"))
					.add("country", rs.getString("country")).build();
				;
			}
		}catch(Exception ex) {
			System.out.println(ex);
		}
		return response;
		
	}
	/*public String putContact() {
		try {
			
		}catch(Exception ex) {
			return "failed";
		}
		
	}
	public String postContact() {
		try {
			
		}catch(Exception ex) {
			return "failed";
		}
		
	}*/
	
}
