package Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@Path("/connection")
public class Connection {
	@GET
	@Produces(MediaType.TEXT_XML)
	public String confirmConn() {
		String message = "<? xml version='1.0' ?> <hello>Connection is working</hello>";
		return message;
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String confirmConnHtml() {
		String message = "<h1>Connection is working</h1>";
		return message;
	}
}
